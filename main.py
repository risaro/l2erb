import requests
import time
import traceback
from lxml import etree

SLEEP_TIMEOUT = 5


def send_update_status(boss_name, boss_status):
    status_str = 'alive!' if boss_status else 'dead.'
    message = f'{boss_name} is {status_str}'
    requests.get(
        f'https://api.telegram.org/'
        f'bot461209453:AAG6fnNGcByN9sbF4wNvmkjIYBqc6bW6GGk/sendMessage?chat_id=@l2_notifs&text={message}')


class BossStatus:
    kabed = False
    barakiel = False
    bariona = False
    karte = False
    verfa = False

    def handle_update(self, status_dict):
        for boss_name, boss_status in status_dict.items():
            if 'kabed' in boss_name.lower():
                if self.kabed != boss_status:
                    self.kabed = boss_status
                    send_update_status('Ghost Knight Kabed', self.kabed)
            elif 'barakiel' in boss_name.lower():
                if self.barakiel != boss_status:
                    self.barakiel = boss_status
                    send_update_status('Flame of Splendor, Barakiel', self.barakiel)
            elif 'bariona' in boss_name.lower():
                if self.bariona != boss_status:
                    self.bariona = boss_status
                    send_update_status('Bariona', self.bariona)
            elif 'karte' in boss_name.lower():
                if self.karte != boss_status:
                    self.karte = boss_status
                    send_update_status('Karte', self.karte)
            elif 'verfa' in boss_name.lower():
                if self.verfa != boss_status:
                    self.verfa = boss_status
                    send_update_status('Verfa', self.verfa)


_bosses = BossStatus()


def get_kabed_status(html_tree):
    kabed_status_xpath = '//main/div/div/div/div[2]/div/ul/li[2]/div/div/span[1]'

    kabed_status = etree.tostring(html_tree.xpath(kabed_status_xpath)[0], encoding='unicode')
    print(f'\tresponse: {kabed_status}')
    if 'респ' not in str.lower(kabed_status):
        return 'alive'

    return 'dead'


def get_statuses(html_tree):
    bosses_names_xpath = '//*[contains(text(), "Barakiel")]/../../li/b'
    resp_timers_xpath = '//*[contains(text(), "Barakiel")]/../../li/span'

    bosses_names = html_tree.xpath(bosses_names_xpath)
    resp_timers = html_tree.xpath(resp_timers_xpath)

    result = {}

    for i in range(0, 5):
        boss_name = bosses_names[i].text
        boss_status = etree.tostring(resp_timers[i], encoding='unicode')
        result[boss_name] = False if 'Respawn' in boss_status else True

    return result


if __name__ == '__main__':
    html_url = 'https://l2e-global.com/forum/'
    print('scanning started')
    while True:
        try:
            html_text = requests.get(html_url).text
            html = etree.HTML(html_text)

            bosses = get_statuses(html)

            _bosses.handle_update(bosses)
        except Exception:
            print(traceback.format_exc())
            raise
        print(f'Iteration ended, sleeping for {SLEEP_TIMEOUT}s...')
        time.sleep(SLEEP_TIMEOUT)
